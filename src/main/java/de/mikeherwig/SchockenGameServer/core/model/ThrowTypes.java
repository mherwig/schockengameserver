package de.mikeherwig.SchockenGameServer.core.model;

public enum ThrowTypes {
	NORMAL, STREET, GENERALS, SCHOCK, SCHOCK_SPARE, SCHOCK_OUT
}
