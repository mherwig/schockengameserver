package de.mikeherwig.SchockenGameServer.core.model;

public class Move {
	
	private int[] dice = new int[3];
	
	private int[] keepingDice = new int[3];
	
	public int[] getDice() {
		return dice;
	}
	
	public void setDice(int[] dice) {
		this.dice = dice;
	}
	
	public int[] getKeepingDice() {
		return keepingDice;
	}
	
	public void setKeepingDice(int[] keepingDice) {
		this.keepingDice = keepingDice;
	}
}
