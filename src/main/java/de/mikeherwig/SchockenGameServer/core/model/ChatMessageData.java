package de.mikeherwig.SchockenGameServer.core.model;

import java.util.ArrayList;

public class ChatMessageData {
	
	private ArrayList<ChatMessage> chatMessages = new ArrayList<>();
	
	public ArrayList<ChatMessage> getChatMessages() {
		return chatMessages;
	}
	
	public void setChatMessages(ArrayList<ChatMessage> chatMessages) {
		this.chatMessages = chatMessages;
	}
}
