package de.mikeherwig.SchockenGameServer.core.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Round {
		
	private int playerIndex;
	
	private int moveIndex;
	
	private int maxMove = 3;
	
	private Map<Integer, List<Move>> moves = new HashMap<Integer, List<Move>>();
	

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getMoveIndex() {
		return moveIndex;
	}

	public void setMoveIndex(int moveIndex) {
		this.moveIndex = moveIndex;
	}

	public int getMaxMove() {
		return maxMove;
	}

	public void setMaxMove(int maxMove) {
		this.maxMove = maxMove;
	}
	
	public Map<Integer, List<Move>> getMoves() {
		return moves;
	}
	
	public void setMoves(Map<Integer, List<Move>> moves) {
		this.moves = moves;
	}
}
