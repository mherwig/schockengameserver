package de.mikeherwig.SchockenGameServer.core.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class ChatMessage {
	
	private Date date = new Date();
	
	private String from;
	
	private String text;
	
	@JsonCreator
	public ChatMessage(String from, String text) {
		this.from = from;
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
