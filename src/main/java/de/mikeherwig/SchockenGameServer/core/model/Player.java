package de.mikeherwig.SchockenGameServer.core.model;

import java.util.UUID;

public class Player {
	
	private String playerId = UUID.randomUUID().toString();
	
	private String name;
	
	private int penalityPoints = 0;
	
	private boolean host = false;
	
	public String getPlayerId() {
		return playerId;
	}
	
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPenalityPoints() {
		return penalityPoints;
	}
	
	public void setPenalityPoints(int penalityPoints) {
		this.penalityPoints = penalityPoints;
	}
	
	public boolean isHost() {
		return host;
	}
	
	public void setHost(boolean host) {
		this.host = host;
	}
}
