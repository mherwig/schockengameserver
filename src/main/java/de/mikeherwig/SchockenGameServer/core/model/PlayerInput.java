package de.mikeherwig.SchockenGameServer.core.model;

public class PlayerInput {
	
	private int[] dice = new int[3];
	
	public int[] getDice() {
		return dice;
	}
	
	public void setDice(int[] dice) {
		this.dice = dice;
	}
}
