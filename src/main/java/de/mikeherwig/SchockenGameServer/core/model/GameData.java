package de.mikeherwig.SchockenGameServer.core.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.mikeherwig.SchockenGameServer.core.eventing.state.GameState;

public class GameData {
	
	private static final int DEFAULT_INITIAL_POINTS = 14;
	
	private String gameInstanceId = UUID.randomUUID().toString();
		
	private int penalityPoints = DEFAULT_INITIAL_POINTS;	
	
	private List<Player> players = new ArrayList<Player>();

	private List<Round> rounds = new ArrayList<Round>();
	
	private int roundIndex;
	
	private boolean finished;
	
	private String currenState = GameState.INITIAL_STATE.name();
	
	public String getGameInstanceId() {
		return gameInstanceId;
	}
	
	public void setGameInstanceId(String gameInstanceId) {
		this.gameInstanceId = gameInstanceId;
	}
	
	public int getPenalityPoints() {
		return penalityPoints;
	}
	
	public void setPenalityPoints(int penalityPoints) {
		this.penalityPoints = penalityPoints;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public List<Round> getRounds() {
		return rounds;
	}
	
	public void setRounds(List<Round> rounds) {
		this.rounds = rounds;
	}
	
	public int getRoundIndex() {
		return roundIndex;
	}
	
	public void setRoundIndex(int roundIndex) {
		this.roundIndex = roundIndex;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	
	public String getCurrenState() {
		return currenState;
	}
	
	public void setCurrenState(String currenState) {
		this.currenState = currenState;
	}
}
