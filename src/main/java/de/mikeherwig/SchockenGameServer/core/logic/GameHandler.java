package de.mikeherwig.SchockenGameServer.core.logic;

import org.apache.commons.lang3.RandomUtils;

import de.mikeherwig.SchockenGameServer.core.logic.helper.GameDataHelper;
import de.mikeherwig.SchockenGameServer.core.model.GameData;
import de.mikeherwig.SchockenGameServer.core.model.Player;

public class GameHandler {
	
	private GameRuleProcessor gameRuleProcessor;
	
	private GameData gameData;
	
	public GameHandler(GameData gameData) {
		this.gameData = gameData;
	}
	
	public void setGameRuleProcessor(GameRuleProcessor gameRuleProcessor) {
		this.gameRuleProcessor = gameRuleProcessor;
	}

	public void rollDice() {
		int[] dice = new int[3];
		
		for (int i = 0; i < 3; i++) {
			dice[i] = RandomUtils.nextInt(1, 7);
		}
				
		GameDataHelper helper = new GameDataHelper(gameData);
		
		helper.getMove().setDice(dice);
		fireRules();
		helper.nextMove();
	}

	public Player createGame(String playerName) {
		Player player = joinGame(playerName);
		player.setHost(true);
		
		return player;
	}

	public Player joinGame(String playerName) {
		Player player = new Player();
		player.setName(playerName);
		
		gameData.getPlayers().add(player);
		
		return player;
	}
	
	private void fireRules() {
		if (gameRuleProcessor != null) {
			gameRuleProcessor.process(gameData);	
		}
	}
}
