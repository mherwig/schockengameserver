package de.mikeherwig.SchockenGameServer.core.logic;

import javax.annotation.PostConstruct;

import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.mikeherwig.SchockenGameServer.core.eventing.GameEventDispatcher;
import de.mikeherwig.SchockenGameServer.core.logic.rules.AlwaysAppliedRule;
import de.mikeherwig.SchockenGameServer.core.logic.rules.SchockOutRule;
import de.mikeherwig.SchockenGameServer.core.messaging.WebSocketController;
import de.mikeherwig.SchockenGameServer.core.model.GameData;

@Component
public class GameRuleProcessor {
	
	@Autowired
	private GameEventDispatcher gameEventDispatcher;
	
	@Autowired
	private WebSocketController webSocketController;
	
	private Rules rules = new Rules();
	
	private RulesEngine rulesEngine = new DefaultRulesEngine();
	
	@PostConstruct
	private void registerRules() {	
        SchockOutRule schockOutRule = new SchockOutRule(gameEventDispatcher);
        schockOutRule.setWebSocketController(webSocketController);
        rules.register(schockOutRule);
        
        AlwaysAppliedRule alwaysAppliedRule = new AlwaysAppliedRule();
        alwaysAppliedRule.setWebSocketController(webSocketController);
        rules.register(alwaysAppliedRule);
	}
	
	public void process(GameData gameData) {
        Facts facts = new Facts();
        facts.put("gameData", gameData);
        
        rulesEngine.fire(rules, facts);
	}
}
