package de.mikeherwig.SchockenGameServer.core.logic.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import de.mikeherwig.SchockenGameServer.core.exception.ThrowTypeNotImplementedException;
import de.mikeherwig.SchockenGameServer.core.model.GameData;
import de.mikeherwig.SchockenGameServer.core.model.Move;
import de.mikeherwig.SchockenGameServer.core.model.Player;
import de.mikeherwig.SchockenGameServer.core.model.Round;
import de.mikeherwig.SchockenGameServer.core.model.ThrowTypes;

public class GameDataHelper {
	
	private GameData data;

	public GameDataHelper(GameData data) {
		this.data = data;
	}

	public Round getRound() {
		List<Round> rounds = data.getRounds();
		
		if (rounds.isEmpty()) {
			Round round = new Round();
			rounds.add(round);
			
			return round;
		}
		
		return rounds.get(data.getRoundIndex());
	}
	
	public Player getPlayer() {
		return data.getPlayers().get(getRound().getPlayerIndex());
	}
	
	private Move getMove(int index) {
		Round round = getRound();
				
		Map<Integer, List<Move>> moves = round.getMoves();
		
		if (moves.isEmpty()) {
			List<Move> list = new ArrayList<Move>();
			Move move = new Move();
			list.add(move);
			moves.put(round.getPlayerIndex(), list);
			
			return move;
		}
		
		return moves.get(round.getPlayerIndex()).get(index);
	}
	
	public Move getMove() {
		Round round = getRound();
		Move move = getMove(round.getMoveIndex());
		
		if (move == null) {
			Move newMove = new Move();
			round.getMoves().get(round.getPlayerIndex()).add(newMove);
			
			return newMove;
		}
		
		return move;		
	}
	
	public int[] getKeepingDice() {
		return getMove().getKeepingDice();
	}

	public ThrowTypes getThrowType() {
		Round round = getRound();
		int moveIndex = round.getMoveIndex();
		
		int[] dice = getMove().getKeepingDice();
		Arrays.sort(dice);
		
		String diceStringValue = Arrays.stream(dice).mapToObj(String::valueOf).collect(Collectors.joining());

		if (diceStringValue.equals("111")) {
			if (moveIndex > 0) {
				Move previousMove = getMove(moveIndex--);
				
				if (previousMove.getKeepingDice().length != 0) {
					return ThrowTypes.SCHOCK_SPARE;
				}
			}
			
			return ThrowTypes.SCHOCK_OUT;
		}
		
		if(Arrays.stream(dice).distinct().count() == 1) {
			return ThrowTypes.GENERALS;
		}
		
		if(diceStringValue.matches("123|456")) {
			return ThrowTypes.STREET;
		}

		return ThrowTypes.NORMAL;
	}
	
	public int getPenalityPoints() {		
		try {
			int[] dices = getKeepingDice();
			ThrowTypes throwType = getThrowType();
			
			switch (throwType) {
			case NORMAL:
				return 1;
			case STREET:
				return 2;
			case GENERALS:
				return 3;
			case SCHOCK:
				return Arrays.stream(dices).max().orElseThrow(NoSuchElementException::new);
			case SCHOCK_SPARE:
			case SCHOCK_OUT:
				return 0;
			default:
				throw new ThrowTypeNotImplementedException();
			}
		} catch (Exception e) {
			throw new RuntimeException("Error, could not calculate penalty points", e);
		}
	}

	public void nextMove() {
		getRound().setMoveIndex(getRound().getMoveIndex() + 1);
	}
}
