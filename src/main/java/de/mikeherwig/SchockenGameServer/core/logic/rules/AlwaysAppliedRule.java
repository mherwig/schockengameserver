package de.mikeherwig.SchockenGameServer.core.logic.rules;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;

import de.mikeherwig.SchockenGameServer.core.messaging.WebSocketController;
import de.mikeherwig.SchockenGameServer.core.model.GameData;


@Rule(name = "AlwaysAppliedRule", description = "Is always successfull, just for testing purpose", priority = 1)
public class AlwaysAppliedRule {
	
	private WebSocketController webSocketController;
	
	public void setWebSocketController(WebSocketController webSocketController) {
		this.webSocketController = webSocketController;
	}

	@Condition
    public boolean when(@Fact("gameData") GameData gameData) {        	 	
    	return true;    	
    }

    @Action
    public void sendMessageToPlayers(@Fact("gameData") GameData gameData) {
    	if (webSocketController != null) {
    		webSocketController.sendServerMessageToSubscribers(gameData.getGameInstanceId(), "Test message from Server");
    	}
    }
}
