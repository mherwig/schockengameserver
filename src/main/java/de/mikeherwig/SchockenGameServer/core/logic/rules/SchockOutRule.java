package de.mikeherwig.SchockenGameServer.core.logic.rules;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;

import de.mikeherwig.SchockenGameServer.core.eventing.GameEventDispatcher;
import de.mikeherwig.SchockenGameServer.core.logic.helper.GameDataHelper;
import de.mikeherwig.SchockenGameServer.core.messaging.WebSocketController;
import de.mikeherwig.SchockenGameServer.core.model.GameData;
import de.mikeherwig.SchockenGameServer.core.model.ThrowTypes;
import de.mikeherwig.SchockenGameServer.core.web.dto.GameDataDto;


@Rule(name = "SchockOut", description = "All three dice show the number 1", priority = 1)
public class SchockOutRule {
		
	private GameEventDispatcher gameEventDispatcher;
	
	private WebSocketController webSocketController;
	
	public SchockOutRule(GameEventDispatcher gameEventDispatcher) {
		this.gameEventDispatcher = gameEventDispatcher;
	}
	
	public void setWebSocketController(WebSocketController webSocketController) {
		this.webSocketController = webSocketController;
	}

	@Condition
    public boolean when(@Fact("gameData") GameData gameData) {        	 	
    	return ThrowTypes.SCHOCK_OUT.equals(new GameDataHelper(gameData).getThrowType());    	
    }

    @Action
    public void then(@Fact("gameData") GameData gameData) throws Exception {
    	gameEventDispatcher.endRound(gameData.getGameInstanceId()).doOnSuccess(data -> sendMessageToPlayers(data));
    }
    
    private void sendMessageToPlayers(GameDataDto data) {
    	if (webSocketController != null) {
    		webSocketController.sendGameDataToSubscribers(data.getGameData().getGameInstanceId(), data.getGameData());
    	}
    }
}
