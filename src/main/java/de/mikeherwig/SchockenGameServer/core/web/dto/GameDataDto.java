package de.mikeherwig.SchockenGameServer.core.web.dto;

import de.mikeherwig.SchockenGameServer.core.model.GameData;

public class GameDataDto {
	
	private String playerId;
	
	private String clientKey;
	
	private GameData gameData;
	
	public String getClientKey() {
		return clientKey;
	}
	
	public void setClientKey(String clientKey) {
		this.clientKey = clientKey;
	}
	
	public GameData getGameData() {
		return gameData;
	}
	
	public void setGameData(GameData gameData) {
		this.gameData = gameData;
	}
	
	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
}
