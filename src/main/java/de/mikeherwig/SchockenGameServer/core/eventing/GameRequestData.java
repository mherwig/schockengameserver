package de.mikeherwig.SchockenGameServer.core.eventing;

import de.mikeherwig.SchockenGameServer.core.model.PlayerInput;

public class GameRequestData {
	
	private String playerId;
	
	private String playerName;
	
	private String gameInstanceId;
	
	private PlayerInput playerInput;
		
	public String getPlayerId() {
		return playerId;
	}
	
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	
	public String getGameInstanceId() {
		return gameInstanceId;
	}
	
	public String getPlayerName() {
		return playerName;
	}
	
	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public void setGameInstanceId(String gameInstanceId) {
		this.gameInstanceId = gameInstanceId;
	}
	
	public PlayerInput getPlayerInput() {
		return playerInput;
	}
	
	public void setPlayerInput(PlayerInput playerInput) {
		this.playerInput = playerInput;
	}
}
