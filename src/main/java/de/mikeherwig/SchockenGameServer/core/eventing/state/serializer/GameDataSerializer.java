package de.mikeherwig.SchockenGameServer.core.eventing.state.serializer;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;

public class GameDataSerializer<T> implements StreamSerializer<T> {

	@Override
	public int getTypeId() {
		return 1;
	}

	@Override
	public void destroy() {		
	}

	@Override
	public void write(ObjectDataOutput out, T object) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		XMLEncoder encoder = new XMLEncoder(bos);
		encoder.writeObject(object);
		encoder.close();
		out.write(bos.toByteArray());
	}

	@SuppressWarnings({ "unchecked", "resource" })
	@Override
	public T read(ObjectDataInput in) throws IOException {
		final InputStream inputStream = (InputStream) in;
        return (T) new XMLDecoder(inputStream).readObject();
	}
}
