package de.mikeherwig.SchockenGameServer.core.eventing.state;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;

@Service
public class PlayerIdentityService {

	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	private Map<String, String> clientKeys;
	
	@PostConstruct
	private void init() {
		clientKeys = hazelcastInstance.getMap("clientkeys");
	}
	
	public String getClientKeyByPlayerId(String playerId) {
		String clientKey = clientKeys.get(playerId);
		
		if (clientKey == null) {
			clientKey = UUID.randomUUID().toString();
			clientKeys.put(playerId, clientKey);
		}
		
		return clientKey;
	}
	
	public String getPlayerIdByClientKey(String clientKey) {
		List<String> list = clientKeys.entrySet().stream().filter(e -> e.getValue().equals(clientKey)).map(e -> e.getKey()).collect(Collectors.toList());
		
		if (!list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
}
