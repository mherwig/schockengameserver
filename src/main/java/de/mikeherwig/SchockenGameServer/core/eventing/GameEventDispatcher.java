package de.mikeherwig.SchockenGameServer.core.eventing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.mikeherwig.SchockenGameServer.core.eventing.state.GameEvent;
import de.mikeherwig.SchockenGameServer.core.eventing.state.PlayerIdentityService;
import de.mikeherwig.SchockenGameServer.core.model.PlayerInput;
import de.mikeherwig.SchockenGameServer.core.web.dto.GameDataDto;
import reactor.core.publisher.Mono;

@Component
public class GameEventDispatcher {
	
	@Autowired
	private GameEventProcessor gameEventProcessor;
	
	@Autowired
	private PlayerIdentityService playerIdentityService;

	public Mono<GameDataDto> createGame(String playerName) throws Exception {
		GameRequestData data = new GameRequestData();
		data.setPlayerName(playerName);
		
		return gameEventProcessor.process(GameEvent.CREATE_GAME, data);		
	}
	
	public Mono<GameDataDto> joinGame(String gameInstanceId, String playerName) throws Exception {
		GameRequestData data = new GameRequestData();
		data.setGameInstanceId(gameInstanceId);
		data.setPlayerName(playerName);
		
		return gameEventProcessor.process(GameEvent.JOIN_GAME, data);	
	}

	public Mono<GameDataDto> startGame(String gameInstanceId, String clientKey) throws Exception {		
		String playerId = playerIdentityService.getPlayerIdByClientKey(clientKey);

		GameRequestData data = new GameRequestData();
		data.setGameInstanceId(gameInstanceId);
		data.setPlayerId(playerId);
		
		return gameEventProcessor.process(GameEvent.START_GAME, data);
	}

	public Mono<GameDataDto> rollDice(String gameInstanceId, String clientKey) throws Exception {
		String playerId = playerIdentityService.getPlayerIdByClientKey(clientKey);

		GameRequestData data = new GameRequestData();
		data.setGameInstanceId(gameInstanceId);
		data.setPlayerId(playerId);
		
		return gameEventProcessor.process(GameEvent.ROLL_DICE, data);
	}

	public Mono<GameDataDto> endTurn(String gameInstanceId, String clientKey, PlayerInput input) throws Exception {
		String playerId = playerIdentityService.getPlayerIdByClientKey(clientKey);

		GameRequestData data = new GameRequestData();
		data.setGameInstanceId(gameInstanceId);
		data.setPlayerId(playerId);
		data.setPlayerInput(input);
		
		return gameEventProcessor.process(GameEvent.END_TURN, data);
	}
	
	public Mono<GameDataDto> endRound(String gameInstanceId) throws Exception {
		GameRequestData data = new GameRequestData();
		data.setGameInstanceId(gameInstanceId);
		
		return gameEventProcessor.process(GameEvent.END_ROUND, data);
	}
}
