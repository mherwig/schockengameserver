package de.mikeherwig.SchockenGameServer.core.eventing.state;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;

import de.mikeherwig.SchockenGameServer.core.model.GameData;

@Component
public class GameContextHandler {

	@Autowired
	private GameDataPersistenceService gameDataPersistenceService;
	
	@Autowired
	private StateMachinePersistenceService stateMachinePersistenceService;
	
	public GameContext createNewContext() throws Exception {
		GameData data = gameDataPersistenceService.createNew();
		StateMachine<GameState, GameEvent> stateMachine = stateMachinePersistenceService.createNew(data.getGameInstanceId());
		
		return new GameContext(stateMachine, data);				
	}
	
	public GameContext restoreContext(String gameInstanceId) throws Exception {
		GameData data = gameDataPersistenceService.find(gameInstanceId);
		StateMachine<GameState, GameEvent> stateMachine = stateMachinePersistenceService.restore(data.getGameInstanceId());
		
		return new GameContext(stateMachine, data);				
	}
	
	public GameContext saveContext(GameContext context) throws Exception {
		GameData data = gameDataPersistenceService.persist(context.getData());
		StateMachine<GameState, GameEvent> stateMachine = stateMachinePersistenceService.persist(context.getStateMachine());
		
		return new GameContext(stateMachine, data);				
	}
}
