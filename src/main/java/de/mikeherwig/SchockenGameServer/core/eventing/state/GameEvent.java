package de.mikeherwig.SchockenGameServer.core.eventing.state;

public enum GameEvent {
	CREATE_GAME, JOIN_GAME, START_GAME, ROLL_DICE, END_TURN, END_ROUND
}
