package de.mikeherwig.SchockenGameServer.core.eventing.state;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;

@Service
public class StateMachinePersistenceService {
	
	@Autowired
    private StateMachineFactory<GameState, GameEvent> stateMachineFactory;
	
	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	private StateMachinePersister<GameState, GameEvent, String> persister;
	
	@PostConstruct
	private void init() {
		persister = new DefaultStateMachinePersister<GameState, GameEvent, String>(new StateMachineMapPersister(hazelcastInstance.getMap("statemachines")));
	}
	
	public StateMachine<GameState, GameEvent> createNew(String gameInstanceId) throws Exception {
		StateMachine<GameState, GameEvent> stateMachine = stateMachineFactory.getStateMachine(gameInstanceId);
		stateMachine.start();
		
		return persist(stateMachine);
	}
		
	public StateMachine<GameState, GameEvent> persist(StateMachine<GameState, GameEvent> stateMachine) throws Exception {
		persister.persist(stateMachine, stateMachine.getId());
		
		return stateMachine;
	}

	public StateMachine<GameState, GameEvent> restore(String gameInstanceId) throws Exception {
		StateMachine<GameState, GameEvent> stateMachine = stateMachineFactory.getStateMachine(gameInstanceId);
		stateMachine = persister.restore(stateMachine, gameInstanceId);
		stateMachine.start();
		
		return stateMachine;
	}
}
