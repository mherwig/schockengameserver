package de.mikeherwig.SchockenGameServer.core.eventing;

import de.mikeherwig.SchockenGameServer.core.model.GameData;
import de.mikeherwig.SchockenGameServer.core.web.dto.GameDataDto;

public class GameDataDtoWrapper {
	
	public static GameDataDto wrap(String playerId, String clientKey, GameData gameData) {
		GameDataDto dto = new GameDataDto();
		dto.setPlayerId(playerId);
		dto.setClientKey(clientKey);
		dto.setGameData(gameData);
		
		return dto;
	}
}
