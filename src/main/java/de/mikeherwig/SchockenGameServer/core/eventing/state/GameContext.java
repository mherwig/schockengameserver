package de.mikeherwig.SchockenGameServer.core.eventing.state;

import org.springframework.statemachine.StateMachine;

import de.mikeherwig.SchockenGameServer.core.model.GameData;

public class GameContext {
	
	private StateMachine<GameState, GameEvent> stateMachine;
	
	private GameData data;
		
	public GameContext(StateMachine<GameState, GameEvent> stateMachine, GameData data) {
		this.stateMachine = stateMachine;
		this.data = data;
	}
	
	public StateMachine<GameState, GameEvent> getStateMachine() {
		return stateMachine;
	}
	
	public void setStateMachine(StateMachine<GameState, GameEvent> stateMachine) {
		this.stateMachine = stateMachine;
	}
	
	public GameData getData() {
		return data;
	}
	
	public void setData(GameData data) {
		this.data = data;
	}
}
