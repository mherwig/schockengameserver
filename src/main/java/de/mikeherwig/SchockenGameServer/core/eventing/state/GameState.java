package de.mikeherwig.SchockenGameServer.core.eventing.state;

public enum GameState {
	INITIAL_STATE, GAME_CREATED, GAME_STARTED, PLAYERS_TURN, END_OF_PLAYERS_TURN, END_OF_ROUND
}
