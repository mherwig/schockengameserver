package de.mikeherwig.SchockenGameServer.core.eventing.state;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;

import de.mikeherwig.SchockenGameServer.core.model.GameData;

@Service
public class GameDataPersistenceService {
	
	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	private Map<String, GameData> games;

	@PostConstruct
	private void init() {
		games = hazelcastInstance.getMap("games");
	}
	
	public GameData createNew() {
		return persist(new GameData());
	}
		
	public GameData persist(GameData data) {
		games.put(data.getGameInstanceId(), data);
		
		return data;
	}

	public GameData find(String key) {
		return games.get(key);
	}
}
