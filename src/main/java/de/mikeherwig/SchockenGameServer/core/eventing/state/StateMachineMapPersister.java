package de.mikeherwig.SchockenGameServer.core.eventing.state;

import java.util.Map;

import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;
import org.springframework.statemachine.support.StateMachineInterceptor;

public class StateMachineMapPersister implements StateMachineRuntimePersister<GameState, GameEvent, String> {
	
	private Map<String, StateMachineContext<GameState, GameEvent>> map;
	
	public StateMachineMapPersister(Map<String, StateMachineContext<GameState, GameEvent>> map) {
		this.map = map;
	}
	
	@Override
	public void write(StateMachineContext<GameState, GameEvent> context, String contextObj) throws Exception {
		map.put(contextObj, context);
	}

	@Override
	public StateMachineContext<GameState, GameEvent> read(String contextObj) throws Exception {
		return map.get(contextObj);
	}

	@Override
	public StateMachineInterceptor<GameState, GameEvent> getInterceptor() {
		// TODO Auto-generated method stub
		return null;
	}
}
