package de.mikeherwig.SchockenGameServer.core.eventing.guards;

import java.util.concurrent.CompletableFuture;

import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

import de.mikeherwig.SchockenGameServer.core.eventing.GameEventProcessor;
import de.mikeherwig.SchockenGameServer.core.eventing.GameRequestData;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameEvent;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameState;
import de.mikeherwig.SchockenGameServer.core.logic.helper.GameDataHelper;
import de.mikeherwig.SchockenGameServer.core.model.GameData;
import de.mikeherwig.SchockenGameServer.core.model.Player;
import de.mikeherwig.SchockenGameServer.core.web.dto.GameDataDto;

public class PlayersTurnGuard implements Guard<GameState, GameEvent> {
	
	private GameEventProcessor gameEventProcessor;
	
	public PlayersTurnGuard(GameEventProcessor gameEventProcessor) {
		this.gameEventProcessor = gameEventProcessor;
	}

	@Override
	public boolean evaluate(StateContext<GameState, GameEvent> context) {
		GameRequestData gameRequestData = (GameRequestData) context.getMessageHeader("gameRequestData");
		GameData gameData = (GameData) context.getMessageHeader("gameData");
		
		Player player = gameData.getPlayers().stream()
				.filter(p -> p.getPlayerId().equals(gameRequestData.getPlayerId()))
				.findFirst()
				.orElse(new Player());
				
		GameDataHelper helper = new GameDataHelper(gameData);
		
		boolean isPlayersTurn = helper.getPlayer().getPlayerId().equals(player.getPlayerId());
		
		if (!isPlayersTurn) {
			CompletableFuture<GameDataDto> future = gameEventProcessor.getFutures().get(gameData.getGameInstanceId());
			
			if (future != null) {
    			future.completeExceptionally(new Exception("It's not your turn!"));
			}
			
			return false;
		}
		
		return true;
	}
}
