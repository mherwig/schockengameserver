package de.mikeherwig.SchockenGameServer.core.eventing;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.stereotype.Component;

import de.mikeherwig.SchockenGameServer.core.eventing.state.GameContext;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameContextHandler;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameEvent;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameState;
import de.mikeherwig.SchockenGameServer.core.eventing.state.PlayerIdentityService;
import de.mikeherwig.SchockenGameServer.core.logic.GameHandler;
import de.mikeherwig.SchockenGameServer.core.logic.GameRuleProcessor;
import de.mikeherwig.SchockenGameServer.core.web.dto.GameDataDto;
import reactor.core.publisher.Mono;

@Component
public class GameEventProcessor extends StateMachineListenerAdapter<GameState, GameEvent> {
	
	private Map<String, CompletableFuture<GameDataDto>> futures = new ConcurrentHashMap<String, CompletableFuture<GameDataDto>>();
	
	@Autowired
	private GameContextHandler gameContextHandler;
	
	@Autowired
	private PlayerIdentityService playerIdentityService;
	
	@Autowired
	private GameRuleProcessor gameRuleProcessor;
	
	private void process(CompletableFuture<GameDataDto> future, GameEvent event, GameRequestData data) throws Exception {
		GameContext context;
		
		if (data.getGameInstanceId() == null) {
			context = gameContextHandler.createNewContext();
		} else {
			context = gameContextHandler.restoreContext(data.getGameInstanceId());
		}
		
		data.setGameInstanceId(context.getData().getGameInstanceId());
		
		context.getStateMachine().addStateListener(this);
				
		Message<GameEvent> message = MessageBuilder.withPayload(event)
				.setHeader("gameRequestData", data)
				.setHeader("gameData", context.getData())
				.build();
		
		futures.put(data.getGameInstanceId(), future);
		
		context.getStateMachine().sendEvent(message);
	}
	
	public Mono<GameDataDto> process(GameEvent event, GameRequestData data) throws Exception {
		
		CompletableFuture<GameDataDto> newFuture = new CompletableFuture<GameDataDto>();
		
		if (data.getGameInstanceId() != null) {
			CompletableFuture<GameDataDto> pendingFuture = futures.get(data.getGameInstanceId());

			if (pendingFuture != null && !(pendingFuture.isDone() || pendingFuture.isCancelled())) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							pendingFuture.get(1, TimeUnit.SECONDS);
							process(newFuture, event, data);
						} catch (Exception e) {
							newFuture.completeExceptionally(e);
						}
					}
				}).run();
				
				return Mono.fromFuture(newFuture);
			}
		}
		
		process(newFuture, event, data);
				
		return Mono.fromFuture(newFuture);
	}
	
	@Override
	public void stateContext(StateContext<GameState, GameEvent> stateContext) {		
		GameRequestData gameRequestData = (GameRequestData) stateContext.getMessageHeader("gameRequestData");
		CompletableFuture<GameDataDto> future = futures.get(gameRequestData.getGameInstanceId());
						
		switch (stateContext.getStage()) {
		case STATE_CHANGED:
			try {
				handleStateChange(future, stateContext, gameRequestData);
			} catch (Exception e) {
				future.completeExceptionally(e);
			} finally {
				futures.remove(gameRequestData.getGameInstanceId());	
			}
			
			break;
		case EVENT_NOT_ACCEPTED:
			future.completeExceptionally(new Exception("Event not accepted"));
			futures.remove(gameRequestData.getGameInstanceId());
		default:
			break;
		}
	}

	private void handleStateChange(CompletableFuture<GameDataDto> future, StateContext<GameState, GameEvent> stateContext, GameRequestData gameRequestData) throws Exception {
		StateMachine<GameState, GameEvent> currentStateMachine = stateContext.getStateMachine();

		GameContext context = gameContextHandler.restoreContext(gameRequestData.getGameInstanceId());
		context.getData().setCurrenState(currentStateMachine.getState().getId().name());
		
		GameEvent event = stateContext.getEvent();
		
		String playerId = gameRequestData.getPlayerId();
				
		if (event != null) {
			GameHandler handler = new GameHandler(context.getData());
			handler.setGameRuleProcessor(gameRuleProcessor);
			
			switch (event) {
			case CREATE_GAME:
				playerId = handler.createGame(gameRequestData.getPlayerName()).getPlayerId();
				
				break;
			case JOIN_GAME:
				playerId = handler.joinGame(gameRequestData.getPlayerName()).getPlayerId();
				
				break;
			case START_GAME:				
				break;
			case ROLL_DICE:
				handler.rollDice();
				
				break;
			case END_TURN:
				break;
			case END_ROUND:
				break;
			default:
				break;
			}
		}
				
		context.getStateMachine().stop();
		context.setStateMachine(currentStateMachine);
		
		gameContextHandler.saveContext(context);
		
		String clientKey = playerIdentityService.getClientKeyByPlayerId(playerId);
		
		future.complete(GameDataDtoWrapper.wrap(playerId, clientKey, context.getData()));
	}
	
	public Map<String, CompletableFuture<GameDataDto>> getFutures() {
		return futures;
	}
}
