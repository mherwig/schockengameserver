package de.mikeherwig.SchockenGameServer.core.eventing.state.serializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.kryo.StateMachineContextSerializer;
import org.springframework.statemachine.support.DefaultStateMachineContext;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;

public class StateMachineSerializer<S, E> implements StreamSerializer<StateMachineContext<S, E>> {
	
	private Kryo kryo;
	
	public StateMachineSerializer() {
		this.initKryo();
	}
	
	private void initKryo() {
		kryo = new Kryo();
		kryo.setRegistrationRequired(false);
	}
		
	@Override
	public int getTypeId() {
		return 2;
	}

	@Override
	public void destroy() {}

	@Override
	public void write(ObjectDataOutput out, StateMachineContext<S, E> object) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		Output output = new Output(bos);
		
		kryo.writeObject(output, object, new StateMachineContextSerializer<S, E>());
		out.write(output.getBuffer());
	}

	@SuppressWarnings("unchecked")
	@Override
	public StateMachineContext<S, E> read(ObjectDataInput in) throws IOException {
		final InputStream inputStream = (InputStream) in;
		Input input = new Input(inputStream);

		return (StateMachineContext<S, E>) kryo.readObject(input, DefaultStateMachineContext.class, new StateMachineContextSerializer<S, E>());
	}
}
