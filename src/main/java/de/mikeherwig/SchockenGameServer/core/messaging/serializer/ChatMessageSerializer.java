package de.mikeherwig.SchockenGameServer.core.messaging.serializer;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;

import de.mikeherwig.SchockenGameServer.core.model.ChatMessageData;

public class ChatMessageSerializer implements StreamSerializer<ChatMessageData> {

	@Override
	public int getTypeId() {
		return 3;
	}

	@Override
	public void destroy() {		
	}

	@Override
	public void write(ObjectDataOutput out, ChatMessageData object) throws IOException {
		byte[] data = new ObjectMapper().writeValueAsBytes(object);
		out.write(data);
	}

	@Override
	public ChatMessageData read(ObjectDataInput in) throws IOException {
		InputStream is = (InputStream) in;

		return new ObjectMapper().readValue(is, ChatMessageData.class);
	}
}
