package de.mikeherwig.SchockenGameServer.core.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import de.mikeherwig.SchockenGameServer.core.model.ChatMessage;
import de.mikeherwig.SchockenGameServer.core.model.GameData;

@Controller
@MessageMapping("game")
public class WebSocketController {
	
	@Autowired
    private SimpMessagingTemplate messagingTemplate;
	
	@Autowired
	private ChatMessageCache chatMessageCache;
	
	@MessageMapping("sendChatMessage.{gameInstanceId}.{playerId}")
	@SendTo("/topic/chat/{gameInstanceId}")
    public ChatMessage sendChatMessage(@DestinationVariable String gameInstanceId, @DestinationVariable String playerId, String text) {
		return chatMessageCache.save(gameInstanceId, new ChatMessage(playerId, text));
    }
	
	public void sendGameDataToSubscribers(String gameInstanceId, GameData gameData) {
		messagingTemplate.convertAndSend("/topic/games/" + gameInstanceId, gameData);
	}
	
	public void sendServerMessageToSubscribers(String gameInstanceId, String text) {
		messagingTemplate.convertAndSend("/topic/servermessages/" + gameInstanceId, text);
	}
}
