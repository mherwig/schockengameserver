package de.mikeherwig.SchockenGameServer.core.messaging;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;

import de.mikeherwig.SchockenGameServer.core.model.ChatMessage;
import de.mikeherwig.SchockenGameServer.core.model.ChatMessageData;

@Service
public class ChatMessageCache {

	@Autowired
	private HazelcastInstance hazelcastInstance;

	private Map<String, ChatMessageData> map;

	@PostConstruct
	private void init() {
		map = hazelcastInstance.getMap("chatmessages");
	}
	
	@PreDestroy
	private void cleanup() {
		hazelcastInstance.shutdown();
	}

	public ChatMessage save(String gameInstanceId, ChatMessage chatMessage) {
		ChatMessageData data = map.get(gameInstanceId);
		
		if (data == null) {
			data = new ChatMessageData();
		}
		
		data.getChatMessages().add(chatMessage);
		map.put(gameInstanceId, data);

		return chatMessage;
	}
	public List<ChatMessage> getAll(String gameInstanceId) {
		ChatMessageData data = map.get(gameInstanceId);
		
		if (data == null) {
			data = new ChatMessageData();
		}
		
		return data.getChatMessages().stream()
				.sorted((e2, e1) -> e1.getDate().compareTo(e2.getDate())).collect(Collectors.toList());
	}
}
