package de.mikeherwig.SchockenGameServer.config;

import java.util.EnumSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import de.mikeherwig.SchockenGameServer.core.eventing.GameEventProcessor;
import de.mikeherwig.SchockenGameServer.core.eventing.guards.HostEventGuard;
import de.mikeherwig.SchockenGameServer.core.eventing.guards.PlayersTurnGuard;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameEvent;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameState;

@Configuration
@EnableStateMachineFactory
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<GameState, GameEvent> {
	
	@Autowired
	private GameEventProcessor gameEventProcessor;
	
    @Override
    public void configure(StateMachineStateConfigurer<GameState, GameEvent> states) throws Exception {
        states.withStates().initial(GameState.INITIAL_STATE).states(EnumSet.allOf(GameState.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<GameState, GameEvent> transitions) throws Exception {
        transitions
            .withExternal()
                .source(GameState.INITIAL_STATE).target(GameState.GAME_CREATED).event(GameEvent.CREATE_GAME)
                .and()
            .withExternal()
                .source(GameState.GAME_CREATED).target(GameState.GAME_CREATED).event(GameEvent.JOIN_GAME)
                .and()
            .withExternal()
                .source(GameState.GAME_CREATED).target(GameState.GAME_STARTED).event(GameEvent.START_GAME).guard(hostEventGuard(gameEventProcessor))
                .and()
        	.withExternal()
        		.source(GameState.GAME_STARTED).target(GameState.PLAYERS_TURN).event(GameEvent.ROLL_DICE).guard(playersTurnGuard(gameEventProcessor))
        		.and()
      		.withExternal()
      			.source(GameState.PLAYERS_TURN).target(GameState.PLAYERS_TURN).event(GameEvent.ROLL_DICE).guard(playersTurnGuard(gameEventProcessor))
      			.and()
        	.withExternal()
				.source(GameState.PLAYERS_TURN).target(GameState.END_OF_PLAYERS_TURN).event(GameEvent.END_TURN).guard(playersTurnGuard(gameEventProcessor))
				.and()
	        .withExternal()
				.source(GameState.END_OF_PLAYERS_TURN).target(GameState.PLAYERS_TURN).event(GameEvent.ROLL_DICE).guard(playersTurnGuard(gameEventProcessor))
				.and()
		    .withExternal()
				.source(GameState.END_OF_PLAYERS_TURN).target(GameState.END_OF_ROUND).event(GameEvent.END_ROUND);
    }
    
    @Bean
    public HostEventGuard hostEventGuard(GameEventProcessor gameEventProcessor) {
        return new HostEventGuard(gameEventProcessor);
    }
    
    @Bean
    public PlayersTurnGuard playersTurnGuard(GameEventProcessor gameEventProcessor) {
        return new PlayersTurnGuard(gameEventProcessor);
    }
}