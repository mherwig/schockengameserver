package de.mikeherwig.SchockenGameServer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachineContext;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.SerializerConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import de.mikeherwig.SchockenGameServer.core.eventing.state.GameEvent;
import de.mikeherwig.SchockenGameServer.core.eventing.state.GameState;
import de.mikeherwig.SchockenGameServer.core.eventing.state.serializer.GameDataSerializer;
import de.mikeherwig.SchockenGameServer.core.eventing.state.serializer.StateMachineSerializer;
import de.mikeherwig.SchockenGameServer.core.messaging.serializer.ChatMessageSerializer;
import de.mikeherwig.SchockenGameServer.core.model.ChatMessageData;
import de.mikeherwig.SchockenGameServer.core.model.GameData;

@Configuration
public class HazelcastConfig {
	
	@Bean
	public HazelcastInstance hazelcastInstance() {
		SerializerConfig sc1 = new SerializerConfig().setImplementation(new GameDataSerializer<GameData>()).setTypeClass(GameData.class);
		SerializerConfig sc2 = new SerializerConfig().setImplementation(new StateMachineSerializer<GameState, GameEvent>()).setTypeClass(StateMachineContext.class);
		SerializerConfig sc3 = new SerializerConfig().setImplementation(new ChatMessageSerializer()).setTypeClass(ChatMessageData.class);
		
		Config config = new Config();
		config.getSerializationConfig().addSerializerConfig(sc1).addSerializerConfig(sc2).addSerializerConfig(sc3);
		
        config.setInstanceName("hazelcast-instance");
 
        {
            MapConfig cfg = new MapConfig();
            cfg.setTimeToLiveSeconds(0);
            cfg.setEvictionPolicy(EvictionPolicy.LRU);
            config.getMapConfigs().put("games", cfg);  	
        }
        {
            MapConfig cfg = new MapConfig();
            cfg.setTimeToLiveSeconds(0);
            cfg.setEvictionPolicy(EvictionPolicy.LRU);
            config.getMapConfigs().put("statemachines", cfg);  	
        }
        {
            MapConfig cfg = new MapConfig();
            cfg.setTimeToLiveSeconds(0);
            cfg.setEvictionPolicy(EvictionPolicy.LRU);
            config.getMapConfigs().put("chatmessages", cfg);  	
        }
        
		return Hazelcast.newHazelcastInstance(config);
	}
}