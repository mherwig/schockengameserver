package de.mikeherwig.SchockenGameServer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebFlux;

/*@Configuration
@EnableSwagger2WebFlux*/
public class SwaggerConfig {
	
	@Bean
    public Docket api() { 
		ApiInfo apiInfo = new ApiInfoBuilder()
				.title("Schocken Demo API")
				.description("Main Game API")
				.version("1").build();
		
		return new Docket(DocumentationType.SWAGGER_2)
		  .groupName("api")
		  .apiInfo(apiInfo)
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("de.syngenio.pwm.SchockenDemo.web.api.v1"))              
          .paths(PathSelectors.any())                          
          .build();                                           
    }
}
