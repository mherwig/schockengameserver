package de.mikeherwig.SchockenGameServer.web.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.mikeherwig.SchockenGameServer.core.eventing.GameEventDispatcher;
import de.mikeherwig.SchockenGameServer.core.model.PlayerInput;
import de.mikeherwig.SchockenGameServer.core.web.dto.GameDataDto;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/game")
public class GameResource {
	
	@Autowired
	private GameEventDispatcher gameEventDispatcher;
		
	@PostMapping("/create")
	private Mono<GameDataDto> create(@RequestParam("playerName") String playerName) throws Exception {
		return gameEventDispatcher.createGame(playerName);
	}
	
	@PostMapping("{gameInstanceId}/join")
	private Mono<GameDataDto> join(@PathVariable("gameInstanceId") String gameInstanceId, @RequestParam("playerName") String playerName) throws Exception {		
		return gameEventDispatcher.joinGame(gameInstanceId, playerName);
	}
	
	@PostMapping("{gameInstanceId}/start")
	private Mono<GameDataDto> start(@PathVariable("gameInstanceId") String gameInstanceId, @RequestParam("clientKey") String clientKey) throws Exception {
		return gameEventDispatcher.startGame(gameInstanceId, clientKey);
	}
	
	@PostMapping("{gameInstanceId}/roll")
	private Mono<GameDataDto> roll(@PathVariable("gameInstanceId") String gameInstanceId, @RequestParam("clientKey") String clientKey) throws Exception {		
		return gameEventDispatcher.rollDice(gameInstanceId, clientKey);
	}
	
	@PostMapping("{gameInstanceId}/endTurn")
	private Mono<GameDataDto> endTurn(@PathVariable("gameInstanceId") String gameInstanceId, @RequestParam("clientKey") String clientKey, @RequestBody PlayerInput input) throws Exception {		
		return gameEventDispatcher.endTurn(gameInstanceId, clientKey, input);
	}
}
