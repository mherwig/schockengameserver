package de.mikeherwig.SchockenGameServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@EnableWebFlux
@SpringBootApplication
public class SchockenGameServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchockenGameServerApplication.class, args);
	}
}
